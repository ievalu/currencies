This app takes exchange rates information from Lietuvos bankas and displays it.

1. To resolve an issue with CORS I used an extension https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi

2. To install dependencies npm install

3. To start the application npm start