import React from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  CircularProgress,
  Snackbar,
  IconButton,
  withStyles,
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import axios from 'axios';
import moment from 'moment-timezone';
import {
  DatePicker,
  Form,
  TableDay,
  TablePeriod,
  Select,
} from 'components';

const parse = require('csv-parse/lib/sync');

const styles = {
  app: {
    margin: '30px',
  },
  button: {
    borderRadius: 3,
    height: 55,
    fontSize: 18,
    fontWeight: 'bold',
    padding: '0 30px',
  },
  spinner: {
    marginLeft: '50%',
    marginTop: '50px',
  },
};

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ratesList: [],
      period: 'day',
      day: moment().format('YYYY-MM-DD'),
      from: moment().format('YYYY-MM-DD'),
      to: moment().format('YYYY-MM-DD'),
      showTable: false,
      currencyCode: '',
      currencies: [],
      loading: false,
      dateChanged: false,
    };
  }

  componentWillMount() {
    const { day } = this.state;
    axios
      .get(`https://www.lb.lt/lt/currency/daylyexport/?csv=1&class=Eu&type=day&date_day=${day}`)
      .then((result) => {
        const resultList = this.parseCurrencies(result);
        this.setState({
          currencies: resultList,
        });
      })
      .catch(err => console.log(err));
  }

  viewRatesDay = () => {
    const { day } = this.state;
    this.setState({ loading: true, ratesList: [] });
    axios
      .get(`https://www.lb.lt/lt/currency/daylyexport/?csv=1&class=Eu&type=day&date_day=${day}`)
      .then((result) => {
        let resultList = this.parseResults(result);
        resultList = this.filterByCurrency(resultList);
        if (resultList.length === 0) {
          this.setState({ day: moment(day).subtract(1, 'd').format('YYYY-MM-DD'), dateChanged: true });
          this.viewRatesDay();
          return;
        }
        this.setState({
          ratesList: resultList,
          showTable: true,
          loading: false,
        });
      })
      .catch(err => console.log(err));
  };

  viewRatesPeriod = () => {
    const {
      from,
      to,
    } = this.state;
    this.setState({ loading: true, ratesList: [] });
    Promise.all([
      axios
        .get(`https://www.lb.lt/lt/currency/daylyexport/?csv=1&class=Eu&type=day&date_day=${from}`),
      axios
        .get(`https://www.lb.lt/lt/currency/daylyexport/?csv=1&class=Eu&type=day&date_day=${to}`)])
      .then((results) => {
        const resultListFrom = this.parseResults(results[0]);
        const resultListTo = this.parseResults(results[1]);
        if (resultListFrom.length === 0) {
          this.setState({ from: moment(from).subtract(1, 'd').format('YYYY-MM-DD'), dateChanged: true });
          this.viewRatesPeriod();
          return;
        }
        if (resultListTo.length === 0) {
          this.setState({ to: moment(to).subtract(1, 'd').format('YYYY-MM-DD'), dateChanged: true });
          this.viewRatesPeriod();
          return;
        }
        let resultList = this.conjuctPeriodData(resultListFrom, resultListTo);
        resultList = this.filterByCurrency(resultList);
        this.setState({
          ratesList: resultList,
          showTable: true,
          loading: false,
        });
      })
      .catch(err => console.log(err));
  };

  parseResults = (result) => {
    const resultList = parse(result.data, {
      columns: true,
      skip_empty_lines: true,
      delimiter: ';',
    });
    for (let i = 0; i < resultList.length; i += 1) {
      resultList[i].Santykis = parseFloat(resultList[i].Santykis.replace(',', '.'));
    }
    return resultList;
  };

  parseCurrencies = (result) => {
    const newResultList = [''];
    const resultList = parse(result.data, {
      columns: true,
      skip_empty_lines: true,
      delimiter: ';',
    });
    for (let i = 0; i < resultList.length; i += 1) {
      newResultList.push(resultList[i]['Valiutos kodas']);
    }
    return newResultList;
  }

  filterByCurrency = (dataList) => {
    const { currencyCode } = this.state;
    if (currencyCode === '') {
      return dataList;
    }
    return dataList.filter(el => el['Valiutos kodas'] === currencyCode);
  };

  conjuctPeriodData = (dataFrom, dataTo) => {
    const resultList = [];
    for (let i = 0; i < dataFrom.length; i += 1) {
      resultList.push({
        'Valiutos pavadinimas': dataFrom[i]['Valiutos pavadinimas'],
        'Valiutos kodas': dataFrom[i]['Valiutos kodas'],
        Santykis: dataTo[i].Santykis,
        'Pokytis vnt': (dataTo[i].Santykis - dataFrom[i].Santykis).toFixed(4),
        'Pokytis procentais': ((dataTo[i].Santykis / dataFrom[i].Santykis - 1) * 100).toFixed(4),
      });
    }
    return resultList;
  };

  closeAlert = () => {
    this.setState({ dateChanged: false });
  }

  selectCurrency = (event) => {
    this.setState({ currencyCode: event.target.value });
  };

  changePeriod = (event) => {
    this.setState({ period: event.target.value, showTable: false, currencyCode: '' });
  };

  setDay = (event) => {
    this.setState({ day: event.target.value });
  };

  setFrom = (event) => {
    this.setState({ from: event.target.value });
  };

  setTo = (event) => {
    this.setState({ to: event.target.value });
  };

  render() {
    const { classes } = this.props;
    const {
      period,
      ratesList,
      day,
      from,
      to,
      showTable,
      currencyCode,
      currencies,
      loading,
      dateChanged,
    } = this.state;
    return (
      <div className={classes.app}>
        <div>
          <Snackbar
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'left',
            }}
            open={dateChanged}
            onClose={this.closeAlert}
            message={<span>The ratio was not published on the date you chose</span>}
            autoHideDuration={6000}
            action={[
              <IconButton
                key="close"
                color="inherit"
                onClick={this.closeAlert}
              >
                <CloseIcon />
              </IconButton>,
            ]}
          />
          {period === 'day'
            && (
              <Button variant="contained" className={classes.button} onClick={this.viewRatesDay}>
                View rates
              </Button>
            )}
          {period === 'period'
            && (
              <Button
                variant="contained"
                className={classes.button}
                onClick={this.viewRatesPeriod}
              >
                View rates
              </Button>
            )}
          <Form period={period} changePeriod={this.changePeriod} />
          <Select
            currencyCode={currencyCode}
            selectCurrency={this.selectCurrency}
            currencies={currencies}
          />
          <DatePicker
            period={period}
            setDay={this.setDay}
            setFrom={this.setFrom}
            setTo={this.setTo}
            fromValue={from}
            toValue={to}
            dayValue={day}
          />
        </div>
        <div>
          {loading && <CircularProgress size={70} className={classes.spinner} />}
        </div>
        {showTable && period === 'day' && <TableDay data={ratesList} />}
        {showTable && period === 'period' && <TablePeriod data={ratesList} date={to} />}
      </div>
    );
  }
}

App.propTypes = {
  classes: PropTypes.shape().isRequired,
};

export default withStyles(styles)(App);
