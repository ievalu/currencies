import React from 'react';
import PropTypes from 'prop-types';
import {
  TextField,
  withStyles,
} from '@material-ui/core';

const styles = {
  textField: {
    marginLeft: '50px',
  },
};

const DatePicker = (props) => {
  const {
    period,
    classes,
    setDay,
    setFrom,
    setTo,
    fromValue,
    toValue,
    dayValue,
  } = props;
  return (
    <span>
      {period === 'day'
          && (
            <TextField
              id="date"
              label="Date"
              type="date"
              onChange={setDay}
              value={dayValue}
              className={classes.textField}
              InputLabelProps={{
                shrink: true,
              }}
            />
          )
      }
      {period === 'period'
          && (
            <span>
              <TextField
                id="from"
                label="From"
                type="date"
                onChange={setFrom}
                value={fromValue}
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
                }}
              />
              <TextField
                id="to"
                label="To"
                type="date"
                onChange={setTo}
                value={toValue}
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </span>
          )
      }
    </span>
  );
};

DatePicker.propTypes = {
  classes: PropTypes.shape().isRequired,
  period: PropTypes.string.isRequired,
  setDay: PropTypes.func.isRequired,
  setFrom: PropTypes.func.isRequired,
  setTo: PropTypes.func.isRequired,
  fromValue: PropTypes.string.isRequired,
  toValue: PropTypes.string.isRequired,
  dayValue: PropTypes.string.isRequired,
};

export default withStyles(styles)(DatePicker);
