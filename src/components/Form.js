import React from 'react';
import PropTypes from 'prop-types';
import {
  FormControl,
  FormControlLabel,
  RadioGroup,
  Radio,
  withStyles,
} from '@material-ui/core';

const styles = {
  formControl: {
    marginLeft: '50px',
  },
};

const Form = (props) => {
  const { classes, period, changePeriod } = props;
  return (
    <FormControl component="fieldset" className={classes.formControl}>
      <RadioGroup
        value={period}
        onChange={changePeriod}
      >
        <FormControlLabel value="day" control={<Radio />} label="One day" />
        <FormControlLabel value="period" control={<Radio />} label="Period" />
      </RadioGroup>
    </FormControl>
  );
};

Form.propTypes = {
  classes: PropTypes.shape().isRequired,
  period: PropTypes.string.isRequired,
  changePeriod: PropTypes.func.isRequired,
};

export default withStyles(styles)(Form);
