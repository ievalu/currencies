import React from 'react';
import PropTypes from 'prop-types';
import {
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  withStyles,
} from '@material-ui/core';

const styles = {
  formControl: {
    minWidth: 160,
    marginLeft: '50px',
  },
};

const CustomSelect = (props) => {
  const {
    classes,
    currencyCode,
    selectCurrency,
    currencies,
  } = props;
  return (
    <FormControl className={classes.formControl}>
      <InputLabel>Currency code</InputLabel>
      <Select
        value={currencyCode}
        onChange={selectCurrency}
      >
        {
          currencies.map(code => (
            <MenuItem key={code} value={code}>{code}</MenuItem>
          ))
        }
      </Select>
    </FormControl>
  );
};

CustomSelect.propTypes = {
  classes: PropTypes.shape().isRequired,
  currencyCode: PropTypes.string.isRequired,
  selectCurrency: PropTypes.func.isRequired,
  currencies: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default withStyles(styles)(CustomSelect);
