import React from 'react';
import PropTypes from 'prop-types';
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
} from '@material-ui/core';

const TableDay = (props) => {
  const { data } = props;
  return (
    <Table>
      <TableHead>
        <TableRow>
          <TableCell>Currency name</TableCell>
          <TableCell>Currency code</TableCell>
          <TableCell>Ratio</TableCell>
          <TableCell>Date</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {data.map(obj => (
          <TableRow key={obj['Valiutos pavadinimas']}>
            <TableCell>{obj['Valiutos pavadinimas']}</TableCell>
            <TableCell>{obj['Valiutos kodas']}</TableCell>
            <TableCell>{obj.Santykis}</TableCell>
            <TableCell>{obj.Data}</TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  );
};

TableDay.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape()).isRequired,
};

export default TableDay;
