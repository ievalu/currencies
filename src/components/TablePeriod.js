import React from 'react';
import PropTypes from 'prop-types';
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
} from '@material-ui/core';

const TablePeriod = (props) => {
  const { data, date } = props;
  return (
    <Table>
      <TableHead>
        <TableRow>
          <TableCell>Currency name</TableCell>
          <TableCell>Currency code</TableCell>
          <TableCell>{`Ratio(${date})`}</TableCell>
          <TableCell>Change, unit</TableCell>
          <TableCell>Change, %</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {data.map(obj => (
          <TableRow key={obj['Valiutos pavadinimas']}>
            <TableCell>{obj['Valiutos pavadinimas']}</TableCell>
            <TableCell>{obj['Valiutos kodas']}</TableCell>
            <TableCell>{obj.Santykis}</TableCell>
            <TableCell>{obj['Pokytis vnt']}</TableCell>
            <TableCell>{obj['Pokytis procentais']}</TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  );
};

TablePeriod.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  date: PropTypes.string.isRequired,
};

export default TablePeriod;
